﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : Singleton<AudioController>
{

    public static AudioController Instance
    {
        get
        {
            return ((AudioController)mInstance);
        }
        set
        {
            mInstance = value;
        }
    }

    public AudioSource effectsAudioSource;
    public AudioSource musicAudioSource;

    [Header("Sound effects")]
    public AudioClip moveSound;
    public AudioClip dropSound;
    public AudioClip rotateSound;
    public AudioClip deleteRowSound;

    public enum Sound
    {
        Move, Drop, Rotate, DeleteRow
    }


    public void PlaySound(Sound sound)
    {
        if (sound == Sound.Drop)
        {
            effectsAudioSource.PlayOneShot(dropSound);
        }
        else if (sound == Sound.Move)
        {
            effectsAudioSource.PlayOneShot(moveSound);
        }
        else if (sound == Sound.Rotate)
        {
            effectsAudioSource.PlayOneShot(rotateSound);
        }
        else if (sound == Sound.DeleteRow)
        {
            effectsAudioSource.PlayOneShot(deleteRowSound);
        }
    }
}
