﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public const int GRID_WIDTH = 10;
    public const int GRID_HEIGHT = 20;
    public const int SCORE_ONE_LINE = 10;

    public float fallingWaitTime = 1;

    [HideInInspector]
    public GameObject currentTetrominoItem;

    public Text scoreText;
    public Transform previewTetrominoPosition;

    public GameObject[] tetrominoPrefs;
    public GameObject[] tetrominoSpritePrefs;

    private Transform[,] _grid = new Transform[GRID_WIDTH, GRID_HEIGHT];

    private int _currentScore = 0;
    private int _nextTetrominoIndex;

    private GameObject _previewTetrominoItem = null;
    private GameObject _ghostTetrominoItem = null;

    private bool gameStarted = false;


    #region UNITY METHODS
    private void Start()
    {
        SpawnNextTetrominoItem();
    }
    #endregion

    #region RAW MANIPULATIONS
    private bool IsRowFullAt(int y)
    {
        for (int x = 0; x < GRID_WIDTH; x++)
        {
            if (_grid[x, y] == null) return false;
        }
        return true;
    }

    private void DeleteMineAt(int y)
    {
        for (int x = 0; x < GRID_WIDTH; x++)
        {
            Destroy(_grid[x, y].gameObject);
            _grid[x, y] = null;
        }
    }

    private void MoveRowDown(int y)
    {
        for (int x = 0; x < GRID_WIDTH; x++)
        {
            if (_grid[x, y] != null)
            {
                _grid[x, y - 1] = _grid[x, y];
                _grid[x, y] = null;
                _grid[x, y - 1].position += new Vector3(0, -1, 0);
            }
        }
    }

    private void MoveAllRowsDown(int y)
    {
        for (int i = y; i < GRID_HEIGHT; i++)
        {
            MoveRowDown(i);
        }

    }

    private void DeleteFullRows()
    {
        bool isAnyRowsToDelete = false;
        for (int y = 0; y < GRID_HEIGHT; y++)
        {
            if (IsRowFullAt(y))
            {
                IncrementScore();
                DeleteMineAt(y);
                MoveAllRowsDown(y + 1);
                isAnyRowsToDelete = true;
                y--;
            }
        }
        if (isAnyRowsToDelete) AudioController.Instance.PlaySound(AudioController.Sound.DeleteRow);
    }
    #endregion

    #region GRID
    public void UpdateGrid(Transform tetrominoItemTransform)
    {
        for (int y = 0; y < GRID_HEIGHT; y++)
        {
            for (int x = 0; x < GRID_WIDTH; x++)
            {
                if (_grid[x, y] != null)
                {
                    if (_grid[x, y].parent == tetrominoItemTransform)
                    {
                        _grid[x, y] = null;
                    }
                }
            }
        }
        foreach (Transform mino in tetrominoItemTransform)
        {
            Vector2 pos = Round(mino.position);
            if (pos.y < GRID_HEIGHT)
            {
                _grid[(int)pos.x, (int)pos.y] = mino;
            }
        }
    }

    public bool IsOverGrid(Transform tetrominoItemTransform)
    {
        foreach (Transform mino in tetrominoItemTransform)
        {
            Vector2 pos = Round(mino.position);
            if (pos.y > GRID_HEIGHT) return true;
        }
        return false;
    }

    public Transform GetTransformAtGridPosition(Vector2 pos)
    {
        if (pos.y > GRID_HEIGHT - 1) return null;
        else return _grid[(int)pos.x, (int)pos.y];
    }

    public bool IsInGrid(Vector2 pos)
    {
        return ((int)pos.x >= 0 && (int)pos.x < GRID_WIDTH && (int)pos.y >= 0);
    }
    #endregion

    #region SCORE
    private void IncrementScore()
    {
        _currentScore += SCORE_ONE_LINE;
        scoreText.text = _currentScore.ToString();
    }

    private int GetBestScore()
    {
        return PlayerPrefs.GetInt("bestScore");
    }

    private void SetBestScore(int score)
    {
        PlayerPrefs.SetInt("bestScore", score);
    }

    private void UpdateBestScore()
    {
        int bestScore = GetBestScore();
        if (bestScore < _currentScore) SetBestScore(_currentScore);
    }
    #endregion

    #region SPAWNERS
    private void SpawnNextTetrominoItem()
    {
        if (!gameStarted)
        {
            gameStarted = true;
            _nextTetrominoIndex = UnityEngine.Random.Range(0, tetrominoPrefs.Length - 1);

            InstantiatePreviewTetromino();
        }
        currentTetrominoItem = Instantiate(tetrominoPrefs[_nextTetrominoIndex], new Vector3(GRID_WIDTH / 2, GRID_HEIGHT, 0), Quaternion.identity) as GameObject;
        InstantiatePreviewTetromino();
    }

    private void InstantiatePreviewTetromino()
    {
        if (_previewTetrominoItem != null)
            Destroy(_previewTetrominoItem);

        _nextTetrominoIndex = UnityEngine.Random.Range(0, tetrominoPrefs.Length - 1);
        _previewTetrominoItem = Instantiate(tetrominoSpritePrefs[_nextTetrominoIndex], previewTetrominoPosition.position, Quaternion.identity) as GameObject;
        _previewTetrominoItem.transform.SetParent(previewTetrominoPosition);
    }
    #endregion

    private void GameOver()
    {
        AppData.Instance.gainedScore = _currentScore;
        UpdateBestScore();
        SceneManager.LoadScene("GameOver");
    }

    public void OnTetrominoDroped(Transform tetromino)
    {
        DeleteFullRows();
        if (IsOverGrid(tetromino)) GameOver();
        else SpawnNextTetrominoItem();
    }

    public Vector2 Round(Vector2 pos)
    {
        return new Vector2(Mathf.Round(pos.x), Mathf.Round(pos.y));
    }
}
