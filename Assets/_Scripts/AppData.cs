﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppData : Singleton<AppData>
{
    public static AppData Instance
    {
        get
        {
            return ((AppData)mInstance);
        }
        set
        {
            mInstance = value;
        }
    }

    public int gainedScore;
}
