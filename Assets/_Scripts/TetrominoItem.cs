﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrominoItem : MonoBehaviour
{

    private GameController _gameController;
    private Action OnComplete;

    private float _retentionHorizontal = 0.1f;
    private float _retentionVertical = 0.01f;
    private float _retentionBtnDown = 0.2f;

    private float _retentionTimerX = 0;
    private float _retentionTimerY = 0;
    private float _retentionTimerBtnDown = 0;

    private bool _moveImmediatelyHorizontal = true;
    private bool _moveImmediatelyVertical = true;

    private IEnumerator fallingThread = null;

    private bool IsAllowedToDrop = true;


    #region UNITY METHODS
    private void Start()
    {
        _gameController = (GameController)FindObjectOfType<GameController>();

        // Initialize the action which will be called at the end of falling
        OnComplete = () =>
        {
            _gameController.OnTetrominoDroped(this.transform);
            Destroy(this);
        };
        fallingThread = Falling(_gameController.fallingWaitTime, OnComplete);

        // Start falling courontine
        StartCoroutine(fallingThread);
    }

    /// <summary>
    /// Listen to kayboard Input
    /// </summary>
    private void Update()
    {
        CheckUserInput();
    }
    #endregion

    #region COMANDS
    private void MoveLeft()
    {
        if (!_moveImmediatelyHorizontal)
        {
            if (_retentionTimerBtnDown < _retentionBtnDown)
            {
                _retentionTimerBtnDown += Time.deltaTime;
                return;
            }

            if (_retentionTimerX < _retentionHorizontal)
            {
                _retentionTimerX += Time.deltaTime;
                return;
            }
            _retentionTimerX = 0;
        }

        if (_moveImmediatelyHorizontal) _moveImmediatelyHorizontal = false;

        transform.localPosition += Movement.StepLeft;
        if (IsValidPosition())
        {
            AudioController.Instance.PlaySound(AudioController.Sound.Move);
            _gameController.UpdateGrid(this.transform);
        }
        else transform.localPosition -= Movement.StepLeft;
    }

    private void MoveRight()
    {
        if (!_moveImmediatelyHorizontal)
        {
            if (_retentionTimerBtnDown < _retentionBtnDown)
            {
                _retentionTimerBtnDown += Time.deltaTime;
                return;
            }


            if (_retentionTimerX < _retentionHorizontal)
            {
                _retentionTimerX += Time.deltaTime;
                return;
            }
            else _retentionTimerX = 0;
        }
        if (_moveImmediatelyHorizontal) _moveImmediatelyHorizontal = false;

        transform.localPosition += Movement.StepRight;
        if (IsValidPosition())
        {
            AudioController.Instance.PlaySound(AudioController.Sound.Move);
            _gameController.UpdateGrid(this.transform);
        }
        else transform.localPosition -= Movement.StepRight;
    }

    private void MoveDown()
    {
        IsAllowedToDrop = false;

        // Stop and clear thread
        StopCoroutine(fallingThread);
        fallingThread = null;

        // Start fast drop action
        StartCoroutine(Drop(OnComplete));
    }

    private void RotateTetromino()
    {
        transform.Rotate(Movement.Rotate);
        if (IsValidPosition())
        {
            AudioController.Instance.PlaySound(AudioController.Sound.Rotate);
            _gameController.UpdateGrid(this.transform);
        }
        else transform.Rotate(-Movement.Rotate);
    }

    private IEnumerator Falling(float waitTime, Action onComplete)
    {
        while (true)
        {
            transform.localPosition += Movement.StepDown;
            if (!IsValidPosition())
            {
                transform.localPosition -= Movement.StepDown;
                break;
            }
            else _gameController.UpdateGrid(this.transform);

            yield return new WaitForSeconds(waitTime);
        }
        onComplete();
    }

    private IEnumerator Drop(Action onComplete)
    {
        while (true)
        {
            yield return null;
            transform.localPosition += Movement.StepDown;
            if (!IsValidPosition())
            {
                transform.localPosition -= Movement.StepDown;
                break;
            }
            else _gameController.UpdateGrid(this.transform);
        }
        AudioController.Instance.PlaySound(AudioController.Sound.Drop);
        onComplete();
    }
    #endregion

    private void CheckUserInput()
    {
        if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
        {
            ResetTimers();
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            MoveLeft();
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            MoveRight();
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            RotateTetromino();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) && IsAllowedToDrop)
        {
            MoveDown();
        }
    }

    private void ResetTimers()
    {
        _retentionTimerX = 0;
        _retentionTimerY = 0;
        _retentionTimerBtnDown = 0;

        _moveImmediatelyHorizontal = true;
        _moveImmediatelyVertical = true;
    }

    private bool IsValidPosition()
    {
        foreach (Transform mino in this.transform)
        {
            Vector2 pos = _gameController.Round(mino.position);
            if (!_gameController.IsInGrid(pos)) return false;
            if (_gameController.GetTransformAtGridPosition(pos) != null && _gameController.GetTransformAtGridPosition(pos).parent != transform) return false;
        }
        return true;
    }
}
