﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI_GameOver : MonoBehaviour
{

    public Text bestScoreText;
    public Text gainedScoreText;


    private void Start()
    {
        bestScoreText.text = PlayerPrefs.GetInt("bestScore").ToString();
        gainedScoreText.text = AppData.Instance.gainedScore.ToString();
    }

    public void TryAgain()
    {
        SceneManager.LoadScene("Game");
    }
}
