﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Movement {
    public static Vector3 StepLeft
    {
        get { return new Vector3(-1, 0, 0); }
    }

    public static Vector3 StepRight
    {
        get { return new Vector3(1, 0, 0); }
    }

    public static Vector3 StepDown
    {
        get { return new Vector3(0, -1, 0); }
    }

    public static Vector3 Rotate
    {
        get { return new Vector3(0, 0, 90); }
    }
}
